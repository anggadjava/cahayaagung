<div class="table-responsive">
    <table id="item_table" class="items table table-striped table-bordered">
        <thead>
        <tr>
            
            <th><?php echo lang('item'); ?></th>
            <th style="min-width: 300px;"><?php echo lang('description'); ?></th>
            <th style="width: 100px;"><?php echo lang('price'); ?></th>
            <th><?php echo lang('total'); ?></th>
            <th>Gambar</th>
            <th>Print Surat Jalan</th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        <tr id="new_item" style="display: none;">
            <td style="vertical-align: top;">
                <input type="hidden" name="invoice_id"
                       value="<?php echo $invoice_id; ?>">
                <input type="hidden" name="item_id" value="" class="form-control">
                <input type="text" name="item_name" class="lookup-item-name form-control"
                       style="width: 90%;" data-typeahead=""><br>
                <label>
                    <input type="checkbox" name="save_item_as_lookup" tabindex="999">
                    <?php echo lang('save_item_as_lookup'); ?>
                </label>
            </td>

            <td>
                <textarea name="item_description" style="width: 90%;" class="form-control"></textarea>
            </td>

            <td style="vertical-align: top;">
                <input type="hidden" class="input-sm form-control"
                       name="item_quantity"  value="1"></td>

            <td style="vertical-align: top;">
                <input type="text" class="input-sm form-control"
                       name="item_price" value=""></td>

            
            <td style="vertical-align: top;"><span name="item_total"></span></td>
            <td></td>
        </tr>
        <?php $i = 1;?>
        <?php foreach ($items as $item) { ?>
            <tr class="item">
                <td style="vertical-align: top;">
                    <input type="hidden" name="invoice_id" value="<?php echo $invoice_id; ?>">
                    <input type="hidden" name="item_id" value="<?php echo $item->item_id; ?>">
                    <input type="text" name="item_name" style="width: 90%;" class="form-control"
                           value="<?php echo $item->item_name; ?>">
                </td>

                <td>
                    <textarea name="item_description" style="width: 90%;" class="form-control"><?php echo $item->item_description; ?></textarea>
                </td>

                <td style="vertical-align: top;">
                <input type="hidden" name="item_quantity" style="width: 90%;" class="form-control"
                           value="1">
                    <input type="text" name="item_price" style="width: 90%;" class="form-control"
                           value="<?php echo format_amount($item->item_price); ?>">
                </td>

                
                <td style="vertical-align: top;">
                <span name="item_total">
                    <?php echo format_currency($item->item_total); ?>
                </span>
                </td>
                <td style="vertical-align: top;">
                    <?php
                        if (!empty($item->item_image)) {
                            if(substr($item->item_image, -3)!='pdf'){
                                echo "<a target='_blank' href='".base_url()."uploads/".$item->item_image."'> <img width=50px height=50px src='".base_url()."uploads/".$item->item_image."'/></a>";    
                            }else{
                                echo "<a target='_blank' href='".base_url()."uploads/".$item->item_image."'>Download PDF</a>";    
                            }
                            
                        }

                     ?>
                    <input type="file" class="item_image" id ="item_image_<?php echo $i;?>" name="upload" style="width: 90%;" class="form-control">
                    <input type="hidden" id ="input_item_image_<?php echo $i;?>" name ="item_image" value = "<?php echo $item->item_image; ?>" />
                </td>
                <td style="vertical-align: top;text-align:center;">
                    <input type="checkbox" name="print_sj" value="1" <?php if($item->print_sj==1) echo 'checked="checked"'; ?> />
                </td>

                <td style="vertical-align: top;">
                    <a href="<?php echo site_url('invoices/delete_item/' . $invoice->invoice_id . '/' . $item->item_id); ?>" title="<?php echo lang('delete'); ?>">
                        <i class="fa fa-trash-o text-danger"></i>
                    </a>
                </td>
            </tr>
        <?php $i++;

        } ?>
        
        
        </form>

        </tbody>

    </table>
</div>

<div class="table-responsive">
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th><?php echo lang('total'); ?></th>
            <th><?php echo lang('paid'); ?></th>
            <th><?php echo lang('balance'); ?></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?php echo format_currency($invoice->invoice_total); ?></td>
            <td><?php echo format_currency($invoice->invoice_paid); ?></td>
            <td><?php echo format_currency($invoice->invoice_balance); ?></td>
        </tr>
        </tbody>
    </table>
</div>