<div class="table-responsive">
    <table class="table table-striped">

        <thead>
        <tr>
            <th><?php echo lang('date'); ?></th>
            <th><?php echo lang('amount'); ?></th>
            <th><?php echo lang('note'); ?></th>
            <th><?php echo lang('options'); ?></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($pembelian as $payment) { ?>
            <tr>
                <td><?php echo date_from_mysql($payment->pembelian_date); ?></td>
                <td><?php echo format_currency($payment->pembelian_amount); ?></td>
                <td><?php echo $payment->pembelian_note; ?></td>
                <td>
                    <div class="options btn-group">
                        <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-cog"></i> <?php echo lang('options'); ?>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo site_url('pembelian/form/' . $payment->pembelian_id); ?>">
                                    <i class="fa fa-edit fa-margin"></i>
                                    <?php echo lang('edit'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('pembelian/delete/' . $payment->pembelian_id); ?>" onclick="return confirm('<?php echo lang('delete_record_warning'); ?>');">
                                    <i class="fa fa-trash-o fa-margin"></i>
                                    <?php echo lang('delete'); ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        <?php } ?>
        </tbody>

    </table>
</div>