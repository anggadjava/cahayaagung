<div class="headerbar">
    <h1>Pembelian</h1>

    <div class="pull-right">
        <a class="btn btn-sm btn-primary" href="<?php echo site_url('pembelian/form'); ?>">
            <i class="fa fa-plus"></i> <?php echo lang('new'); ?>
        </a>
    </div>

    <div class="pull-right">
        <?php echo pager(site_url('pembelian/index'), 'mdl_pembelian'); ?>
    </div>

</div>

<div class="table-content">

    <?php $this->layout->load_view('layout/alerts'); ?>

    <div id="filter_results">
        <?php $this->layout->load_view('pembelian/partial_pembelian_table'); ?>
    </div>

</div>