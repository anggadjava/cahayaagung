<script type="text/javascript">
    $(function() {
        $('#invoice_id').focus();
    });
</script>

<form method="post" class="form-horizontal">

    <?php if ($pembelian_id) { ?>
        <input type="hidden" name="pembelian_id" value="<?php echo $pembelian_id; ?>">
    <?php } ?>

    <div class="headerbar">
        <h1>Form Pembelian</h1>
        <?php $this->layout->load_view('layout/header_buttons'); ?>
    </div>

    <div class="content">

        <?php $this->layout->load_view('layout/alerts'); ?>

        <div class="form-group has-feedback">
            <div class="col-xs-12 col-sm-2 text-right text-left-xs">
                <label for="pembelian_date" class="control-label"><?php echo lang('date'); ?></label>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="date datepicker">
                    <input type="text" name="pembelian_date" id="pembelian_date"
                           class="form-control datepicker"
                           value=""
                           readonly="readonly">
                    <span class="form-control-feedback">
                        <i class="fa fa-calendar"></i>
                    </span>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12 col-sm-2 text-right text-left-xs">
                <label for="pembelian_amount" class="control-label"><?php echo lang('amount'); ?></label>
            </div>
            <div class="col-xs-12 col-sm-6">
                <input type="text" name="pembelian_amount" id="pembelian_amount" class="form-control"
                       value="<?php echo format_amount($this->mdl_pembelian->form_value('pembelian_amount')); ?>">
            </div>
        </div>

        
        <div class="form-group">
            <div class="col-xs-12 col-sm-2 text-right text-left-xs">
                <label for="pembelian_note" class="control-label"><?php echo lang('note'); ?></label>
            </div>
            <div class="col-xs-12 col-sm-6">
                <textarea name="pembelian_note" class="form-control"><?php echo $this->mdl_pembelian->form_value('pembelian_note'); ?></textarea>
            </div>

        </div>
    </div>

</form>