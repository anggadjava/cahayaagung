<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * InvoicePlane
 * 
 * A free and open source web based invoicing system
 *
 * @package     InvoicePlane
 * @author      Kovah (www.kovah.de)
 * @copyright   Copyright (c) 2012 - 2014 InvoicePlane.com
 * @license     https://invoiceplane.com/license.txt
 * @link        https://invoiceplane.com
 * 
 */

class Mdl_Pembelian extends Response_Model {

    public $table            = 'ip_pembelian';
    public $primary_key      = 'ip_pembelian.pembelian_id';
    public $validation_rules = 'validation_rules';

    public function default_select()
    {
        $this->db->select("
            ip_pembelian.*", FALSE);
    }

    public function default_order_by()
    {
        $this->db->order_by('ip_pembelian.pembelian_date DESC');
        $this->db->order_by('pembelian_id','DESC');
    }

    public function default_join()
    {
        
    }

    public function validation_rules()
    {
        return array(
            'pembelian_date'      => array(
                'field' => 'pembelian_date',
                'label' => lang('date'),
                'rules' => 'required'
            ),
            'pembelian_amount'    => array(
                'field' => 'pembelian_amount',
                'label' => 'Pembelian',
                'rules' => 'required'
            ),
            'pembelian_note'      => array(
                'field' => 'pembelian_note',
                'label' => lang('note')
            )
        );
    }

    

    public function save($id = NULL, $db_array = NULL)
    {
        $db_array = ($db_array) ? $db_array : $this->db_array();

        // Save the payment
        $id = parent::save($id, $db_array);
        
        return $id;
    }

    public function delete($id = NULL)
    {
        // Get the invoice id before deleting payment
        $this->db->select('invoice_id');
        $this->db->where('payment_id', $id);
        $invoice_id = $this->db->get('ip_payments')->row()->invoice_id;

        // Delete the payment
        parent::delete($id);

        // Recalculate invoice amounts
        $this->load->model('invoices/mdl_invoice_amounts');
        $this->mdl_invoice_amounts->calculate($invoice_id);

        // Change invoice status back to sent
        $this->db->select('invoice_status_id');
        $this->db->where('invoice_id', $invoice_id);
        $invoice = $this->db->get('ip_invoices')->row();
        
        if ($invoice->invoice_status_id == 4)
        {
            $this->db->where('invoice_id', $invoice_id);
            $this->db->set('invoice_status_id', 2);
            $this->db->update('ip_invoices');
        }
        
        $this->load->helper('orphan');
        delete_orphans();
    }

    public function db_array()
    {
        $db_array = parent::db_array();
        
        $db_array['pembelian_date'] = date_to_mysql($db_array['pembelian_date']);
        $db_array['pembelian_amount'] = standardize_amount($db_array['pembelian_amount']);

        return $db_array;
    }

    public function prep_form($id = NULL)
    {
        if (!parent::prep_form($id))
        {
            return FALSE;
        }

        if (!$id)
        {
            parent::set_form_value('pembelian_date', date('Y-m-d'));
        }
        
        return TRUE;
    }

}

?>