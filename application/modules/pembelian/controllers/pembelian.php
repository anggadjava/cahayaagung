<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * InvoicePlane
 * 
 * A free and open source web based invoicing system
 *
 * @package		InvoicePlane
 * @author		Kovah (www.kovah.de)
 * @copyright	Copyright (c) 2012 - 2014 InvoicePlane.com
 * @license		https://invoiceplane.com/license.txt
 * @link		https://invoiceplane.com
 * 
 */

class Pembelian extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('mdl_pembelian');
    }

    public function index($page = 0)
    {
        $this->mdl_pembelian->paginate(site_url('pembelian/index'), $page);
        $pembelian = $this->mdl_pembelian->result();

        $this->layout->set(
            array(
                'pembelian'           => $pembelian,
                'filter_display'     => TRUE,
                'filter_placeholder' => 'Filter Pembelian',
                'filter_method'      => 'filter_pembelian'
            )
        );

        $this->layout->buffer('content', 'pembelian/index');
        $this->layout->render();
    }

    public function form($id = NULL)
    {
        if ($this->input->post('btn_cancel'))
        {
            redirect('pembelian');
        }

        if ($this->mdl_pembelian->run_validation())
        {
            $id = $this->mdl_pembelian->save($id);

            redirect('pembelian');
        }

        
        $this->layout->set(
            array(
                'pembelian_id'      => $id                
            )
        );

        if ($id)
        {
            $this->layout->set('pembelian', $this->mdl_pembelian->where('ip_pembelian.pembelian_id', $id)->get()->row());
        }

        $this->layout->buffer('content', 'pembelian/form');
        $this->layout->render();
    }

    public function delete($id)
    {
        $this->mdl_payments->delete($id);
        redirect('payments');
    }

}

?>