<?php 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=report_pembelian.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
		<table>
			<tr>
				<th><?php echo lang('date'); ?></th>
				<th><?php echo lang('note'); ?></th>
				<th><?php echo lang('amount'); ?></th>
			</tr>
			<?php foreach ($results as $result) { ?>
			<tr>
				<td><?php echo date_from_mysql($result->pembelian_date, TRUE); ?></td>
				<td><?php echo nl2br($result->pembelian_note); ?></td>
				<td><?php echo format_currency($result->pembelian_amount); ?></td>
			</tr>
			<?php } ?>
		</table>