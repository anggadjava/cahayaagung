<html>
	<head>
		<title><?php echo lang('sales_by_client'); ?></title>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/default/css/reports.css" type="text/css">
	</head>
	<body>
		
		<h3 class="report_title">Laporan Penjualan</h3>
		<table>
			<tr>
				<th>Tanggal</th>
				<th>No Invoice</th>
				<th>Nama Client</th>
				<th>Total</th>
				<th>Telah Dibayar</th>
				<th>Saldo</th>
			</tr>
			<?php 
			$it= 0;
			$ip= 0;
			$ib= 0;
			foreach ($results as $result) { ?>
			<tr>
				<td><?php echo date_from_mysql($result->invoice_date_created, TRUE); ?></td>
				<td><?php echo $result->invoice_number; ?></td>
				<td><?php echo $result->client_name; ?></td>
				<td><?php echo format_currency($result->invoice_total); ?></td>
				<td><?php echo format_currency($result->invoice_paid); ?></td>
				<td><?php echo format_currency($result->invoice_balance); ?></td>
			</tr>
			<?php
			$it = $it + $result->invoice_total;
			$ip = $ip + $result->invoice_paid;
			$ib = $ib + $result->invoice_balance;
			 } ?>
			 <b>
			 <tr>
				<td colspan="3">Total</td>
				<td><?php echo format_currency($it); ?></td>
				<td><?php echo format_currency($ip); ?></td>
				<td><?php echo format_currency($ib); ?></td>
			</tr>
			</b>
		</table>
</body>
</html>