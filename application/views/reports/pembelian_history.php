<html>
	<head>
		<title>History Pembelian</title>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/default/css/reports.css" type="text/css">
	</head>
	<body>
		
		<h3 class="report_title">History Pembelian</h3>
		
		<table>
			<tr>
				<th><?php echo lang('date'); ?></th>
				<th><?php echo lang('note'); ?></th>
				<th class="amount"><?php echo lang('amount'); ?></th>
			</tr>
			<?php foreach ($results as $result) { ?>
			<tr>
				<td><?php echo date_from_mysql($result->pembelian_date, TRUE); ?></td>
				<td><?php echo nl2br($result->pembelian_note); ?></td>
				<td class="amount"><?php echo format_currency($result->pembelian_amount); ?></td>
			</tr>
			<?php } ?>
		</table>
	</body>
</html>