<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/default/css/style.css">
        
        <style>
            body {
                color: #000;
                font-size: 12px;
                font-family: Arial, Verdana, sans-serif;
            }
            table {
                width:100%;
                border-spacing:0;
                border-collapse: collapse;
            }

            .text-right {
                text-align: right;
            }

            .color-l { color: #aaa; }
            .color-n { color: #000; }
            .color-d { color: #000; }

            .border-bottom-l {
                border-bottom-width: 1px;
                border-style: solid;
                border-color: #aaa;
            }
            .border-bottom-n {
                border-bottom-width: 1px;
                border-style: solid;
                border-color: #888;
            }
            .border-bottom-d {
                border-bottom-width: 1px;
                border-style: solid;
                border-color: #555;
            }

            .border-top-l {
                border-top-width: 1px;
                border-style: solid;
                border-color: #aaa;
            }
            .border-top-n {
                border-top-width: 1px;
                border-style: solid;
                border-color: #888;
            }
            .border-top-d {
                border-top-width: 1px;
                border-style: solid;
                border-color: #555;
            }

            .background-l { background-color: #eee; }

            #header table {
                width:100%;
                padding: 0;
            }

            .company-details,
            .company-details h3,
            .company-details p,
            .invoice-details {
                text-align: right;
            }

            .company-name,
            .invoice-id {
                color: #333 !important;
            }
            .invoice-details td {
                padding: 0.2em 0.3em;
            }
            .invoice-items td,
            .invoice-items th {
                padding: 0.2em 0.4em 0.4em;
            }

            .seperator {
                height: 25px;
                margin-bottom: 15px;
            }
            .border-bottom-l p{
                font-size: 10px;
            }
            .term {
                font-size: 9px;
            }


        </style>
        
	</head>
	<body>
        <div id="header">
            <table>
                <tr>
                    <td rowspan="2" style="width:70%">
                        <div style="display: block; height: 2cm"></div>
                        <div class="company-details">
                            <?php echo invoice_logo_pdf(); ?>
                            <h3 class="company-name text-right">
                            </h3>
                            <h2 style="color:red;">CAHAYA AGUNG</h2>
                            <p class="text-right">                                
                                PEMBUATAN, PEMASANGAN & SERVICE
                            </p>
                            <p>KACA, BAJA RINGAN, ALUMINIUM, STAINLESS STEEL, BESI</p>
                            <p>Jl Letjend Suprapto No 21 Jakarta Pusat</p>
                            <p>(Samping Showroom Mobil Suzuki)</p>
                            <br />
                            <p>Telp. (021) 4256731, 4256737 Fax. 4256752 Pin. 76213196</p>
                            <p>Email : info@cahaya-agung.co.id</p>
                        </div>
                        

                    </td>
                    <td style="width:30%;"><p>Jakarta, <?php echo date_from_mysql($invoice->invoice_date_created, TRUE); ?></p></td>
                </tr>
                <tr>
                    <td>
                    <div class="invoice-to">
                            
                            <p>Kepada Yth. :</p>
                            <p>Bpk/Ibu <b><?php echo $invoice->client_name; ?></b><br/>
                                <?php if ($invoice->client_address_1) {
                                    echo $invoice->client_address_1 . '<br/>';
                                } ?>
                                <?php if ($invoice->client_address_2) {
                                    echo $invoice->client_address_2 . '<br/>';
                                } ?>
                                <?php if ($invoice->client_city) {
                                    echo $invoice->client_city . ' ';
                                } ?>
                                <?php if ($invoice->client_zip) {
                                    echo $invoice->client_zip . '<br/>';
                                } ?>
                                <?php if ($invoice->client_state) {
                                    echo $invoice->client_state . '<br/>';
                                } ?>

                                <?php if ($invoice->client_phone) { ?>
                                    <abbr>P:</abbr><?php echo $invoice->client_phone; ?><br/>
                                <?php } ?>
                            </p>
                        </div>
                        
                        <br/>

                    </td>
                </tr>
            </table>
        </div>

        <br/>
        <h2 class="invoice-id">No <?php echo $invoice->invoice_number; ?></h2>
        <br/>

        <div class="invoice-items">
            <table class="table table-striped" style="width: 100%;">
                <thead>
                    <tr class="border-bottom-d">
                        <th class="color-d"><?php echo lang('item'); ?></th>
                        <th class="color-d"><?php echo lang('description'); ?></th>
                        <th class="text-right color-d"><?php echo lang('qty'); ?></th>
                        <th class="text-right color-d"><?php echo lang('price'); ?></th>
                        <th class="text-right color-d"><?php echo lang('total'); ?></th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $linecounter = 0;
                    foreach ($items as $item) { ?>
                        <tr class="border-bottom-n <?php echo ($linecounter % 2 ? 'background-l' : '')?>">
                            <td><?php echo $item->item_name; ?></td>
                            <td><?php echo nl2br($item->item_description); ?></td>
                            <td class="text-right">
                                <?php echo format_amount($item->item_quantity); ?>
                            </td>
                            <td class="text-right">
                                <?php echo format_currency($item->item_price); ?>
                            </td>
                            <td class="text-right">
                                <?php echo format_currency($item->item_subtotal); ?>
                            </td>
                        </tr>
                        <?php $linecounter++; ?>
                    <?php } ?>

                </tbody>
            </table>
            <table>
                <tr>
                    <td class="text-right">
                        <table class="amount-summary">
                            <?php if ($invoice->invoice_item_tax_total > 0) { ?>
                                <tr>
                                    <td class="text-right color-n">
                                        <?php echo lang('item_tax'); ?>
                                    </td>
                                    <td class="text-right color-n">
                                        <?php echo format_currency($invoice->invoice_item_tax_total); ?>
                                    </td>
                                </tr>
                            <?php } ?>

                            <?php foreach ($invoice_tax_rates as $invoice_tax_rate) : ?>
                                <tr>
                                    <td class="text-right color-n">
                                        <?php echo $invoice_tax_rate->invoice_tax_rate_name . ' ' . $invoice_tax_rate->invoice_tax_rate_percent; ?>%
                                    </td>
                                    <td class="text-right color-n">
                                        <?php echo format_currency($invoice_tax_rate->invoice_tax_rate_amount); ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>

                            <tr class="amount-total">
                                <td class="text-right color-d">
                                    <?php echo lang('total'); ?>:
                                </td>
                                <td class="text-right color-d">
                                    <?php echo format_currency($invoice->invoice_total); ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right color-d">
                                    <?php echo lang('paid'); ?>:
                                </td>
                                <td class="text-right color-d">
                                    <?php echo format_currency($invoice->invoice_paid) ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right color-d">
                                    <b><?php echo lang('balance'); ?>:</b>
                                </td>
                                <td class="text-right color-d">
                                    <b><?php echo format_currency($invoice->invoice_balance) ?></b>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <?php if ($invoice->invoice_terms) { ?>
                <h4>Notes</h4>
                <p><?php echo nl2br($invoice->invoice_terms); ?></p>
            <?php } ?>

            <div class="seperator border-bottom-l"></div>
            <table style="width:100%">
                <tr>
                <td style="width:25%;text-align:center;vertical-align:top;">
                    Tanda Terima <br /><br /><br /><br />
                    Menyetujui

                </td>
                <td style="width:50%;vertical-align:top;">
                    <div class="term">
                        <b>Sistem Transaksi - III</b>
                        <p>1.  DP order setelah harga sepakat sebesar <b>50%</b> (Sistem I)</p>
                        <p>2.  (Progress) <b>DP II tambah 45% total 95%</b> dari nilai<br/>
                        proyek (besar maupun kecil) <b>sebelum proyek dikerjakan(Sistem II)</b>
                        </p>
                        <p>3.  Pelunasan <b>Sisa 5% tepat waktu</b> setelah proyek selesai <b>(Sistem III)</b></p>
                        <p>4.  Jika Transaksi dibatalkan Costumer maka DP tidak dapat dikembalikan</p>
                    </div>
                    </td>
                    <td style="text-align:center;width:25%;vertical-align:top;">Hormat Kami,</td>
                </tr>          
            </table>
            
        </div>
	</body>
</html>