<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/default/css/style.css">
        
        <style>
            body {
                color: #000;
                font-size: 12px;
                font-family: Arial, Verdana, sans-serif;
            }
            table {
                width:100%;
                border-spacing:0;
                border-collapse: collapse;
            }

            .text-right {
                text-align: right;
            }

            .color-l { color: #aaa; }
            .color-n { color: #000; }
            .color-d { color: #000; }

            .border-bottom-l {
                border-bottom-width: 1px;
                border-style: solid;
                border-color: #aaa;
            }
            .border-bottom-n {
                border-bottom-width: 1px;
                border-style: solid;
                border-color: #888;
            }
            .border-bottom-d {
                border-bottom-width: 1px;
                border-style: solid;
                border-color: #555;
            }

            .border-top-l {
                border-top-width: 1px;
                border-style: solid;
                border-color: #aaa;
            }
            .border-top-n {
                border-top-width: 1px;
                border-style: solid;
                border-color: #888;
            }
            .border-top-d {
                border-top-width: 1px;
                border-style: solid;
                border-color: #555;
            }

            .background-l { background-color: #eee; }

            #header table {
                width:100%;
                padding: 0;
            }

            .company-details,
            .company-details h3,
            .company-details p,
            .invoice-details {
                text-align: right;
            }

            .company-name,
            .invoice-id {
                color: #333 !important;
            }
            .invoice-details td {
                padding: 0.2em 0.3em;
            }
            .invoice-items td,
            .invoice-items th {
                padding: 0.2em 0.4em 0.4em;
            }

            .seperator {
                height: 25px;
                margin-bottom: 15px;
            }
            .border-bottom-l p{
                font-size: 10px;
            }
            .term {
                font-size: 9px;
            }


        </style>
        
    </head>
    <body>
        <div id="header">
             <table>
                <tr>
                    <td rowspan="2" style="width:20%"><?php echo invoice_logo_pdf(); ?></td>
                    <td rowspan="2" style="width:50%">
                        <div style="display: block; height: 2cm"></div>
                        <div class="company-details">
                            <h3 class="company-name text-right">
                            </h3>
                            <h2 style="color:red;">CAHAYA AGUNG</h2>
                            <p class="text-right">                                
                                PEMBUATAN, PEMASANGAN & SERVICE
                            </p>
                            <p>KACA, BAJA RINGAN, ALUMINIUM, STAINLESS STEEL, BESI</p>
                            <p>Jl Letjend Suprapto No 21 Jakarta Pusat</p>
                            <p>(Samping Showroom Mobil Suzuki)</p>
                            <br />
                            <p>Telp. (021) 4256731, 4256737 Fax. 4256752 Pin. 76213196</p>
                            <p>Email : info@cahaya-agung.co.id</p>
                        
                        </div>
                        

                    </td>
                    <td style="width:30%;padding-left:20px;vertical-align:top;"><p>Jakarta, <?php echo date_from_mysql($invoice->invoice_date_created, TRUE); ?></p></td>
                </tr>
                <tr>
                    <td valign="top" style="padding-left:20px;vertical-align:top;">
                    <div class="invoice-to">
                            
                            <p>Kepada Yth. :</p>
                            <p>Bpk/Ibu <b><?php echo $invoice->client_name; ?></b><br/>
                                <?php if ($invoice->client_address_1) {
                                    echo $invoice->client_address_1 . '<br/>';
                                } ?>
                                <?php if ($invoice->client_address_2) {
                                    echo $invoice->client_address_2 . '<br/>';
                                } ?>
                                <?php if ($invoice->client_city) {
                                    echo $invoice->client_city . ' ';
                                } ?>
                                <?php if ($invoice->client_zip) {
                                    echo $invoice->client_zip . '<br/>';
                                } ?>
                                <?php if ($invoice->client_state) {
                                    echo $invoice->client_state . '<br/>';
                                } ?>

                                <?php if ($invoice->client_phone) { ?>
                                    <abbr>P:</abbr><?php echo $invoice->client_phone; ?><br/>
                                <?php } ?>
                            </p>
                        </div>
                        
                        <br/>

                    </td>
                </tr>
            </table>
        </div>

        <br/>
        <h2 class="invoice-id">Surat Jalan No. <?php echo $invoice->invoice_number; ?></h2>
        <br/>

        <div class="invoice-items">
            <table class="table table-striped" style="width: 100%;">
                <thead>
                    <tr class="border-bottom-d">
                        <th class="color-d"><?php echo lang('item'); ?></th>
                        <th class="color-d"><?php echo lang('description'); ?></th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $linecounter = 0;
                    foreach ($items as $item) { ?>
                        <tr class="border-bottom-n <?php echo ($linecounter % 2 ? 'background-l' : '')?>">
                            <td><?php echo $item->item_name; ?></td>
                            <td><?php echo nl2br($item->item_description); ?></td>
                        </tr>
                    <?php } ?>

                </tbody>
            </table>
            <table style="width: 100%; text-align:center;vertical-align:bottom">
                <tr>
                    <td>Supir</td>
                    <td>Tanda Terima</td>
                    <td>Hormat Kami</td>
                </tr>
                <tr>
                    <td style="height:30px"></td>
                    <td style="height:30px"></td>
                    <td style="height:30px"></td>
                </tr>
                <tr>
                    <td>(......................)</td>
                    <td>(......................)</td>
                    <td>(......................)</td>
                </tr>

            </table>
            
        </div>
    </body>
</html>